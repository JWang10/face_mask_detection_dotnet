using System;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.Dnn;

namespace Geosat.Vision
{
     public class FaceMask
    {
        private VideoCapture _capture = null;
        private Mat _frame { get; set; }
        private bool check = false;
        // private Window window = null;
        private OpenCvSharp.Dnn.Net _faceNet = null;
        private OpenCvSharp.Dnn.Net _maskNet = null;
        public double face_confidence = 0.6;

        private int _faceCount = 0;
        private int _withoutMaskCount = 0;
        private int _last_withoutMaskCount = 0;
        private int _last_faceCount = 0;
        private bool _withoutMaskSignal = false;
        private bool _signal_close = false;

        public FaceMask()
        {
            const string protoTxt = @"face_detector\face.prototxt";
            const string caffeModel = @"face_detector\face.caffemodel";
            const string faceMaskModel = @"mask_detector\mask.model";
            Console.WriteLine("[INFO] loading face model...");
            _faceNet = CvDnn.ReadNetFromCaffe(protoTxt, caffeModel);
            Console.WriteLine("[INFO] loading mask model...");
            _maskNet = CvDnn.ReadNetFromTensorflow(faceMaskModel);
            //     Console.WriteLine("Layer names: {0}", string.Join(", ", _maskNet.GetLayerNames()));        }
        }
        public string StartCamera(int cameraIndex = 0, string cameraMode = "")
        {

            try
            {
                if (_capture != null) _capture.Dispose();
                _capture = new VideoCapture(cameraIndex);
                // _capture = new VideoCapture(cameraIndex, VideoCaptureAPIs.DSHOW);

                _frame = new Mat();
                _capture.FrameWidth = 640;
                _capture.FrameHeight = 480;
                _capture.AutoFocus = true;
                // int _fps = (int)_capture.Fps;
                // int interval = (int)(1000 / _fps);
                // framenum = 1;
                if (!cameraMode.Contains("noshow"))
                    Cv2.NamedWindow("Capture");
                // window = new Window("capture");
                while (true)
                {
                    _capture.Read(_frame);
                    if (_frame.Empty())
                        return ("No data");
                    Cv2.Flip(_frame, _frame, FlipMode.Y);
                    int frameHeight = _frame.Rows;
                    int frameWidth = _frame.Cols;
                    /// Image processing
                    _frame = ImageProcess(_frame);
                    ///
                    if (!cameraMode.Contains("noshow"))
                        Cv2.ImShow("Capture", _frame);
                    // window.ShowImage(_frame);
                    // if (framenum >= _fps)
                    // {
                    //     framenum = 0;
                    // }
                    int c = Cv2.WaitKey(1) & 0xff;
                    if (c == 27)
                    {
                        break;
                    }
                    //if (closeCamera(c)) // escape
                    // if (check) // escape
                    // {
                    //     check = false;
                    //     if (!cameraMode.Contains("noshow"))
                    //         Cv2.DestroyAllWindows();
                    //     _capture.Release();
                    //     _capture.Dispose();
                    //     //framenum = 0;
                    //     break;
                    // }
                    // framenum++;


                }
                //_bitmapFrame = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(_frame);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return ex.Message;
            }

            return "Camera was activated";
        }
        public string StopCamera()
        {
            if (_capture != null)
            {
                _signal_close = true;
                closeCamera('\x1b');
                _capture = null;
            }
            else
                return "Camera wasn't activated";
            return "Camera was shutdown";
        }
        private bool closeCamera(int c)
        {
            check = false;
            if (c == '\x1b') //esc
                check = true;

            return check;
        }
        /// <summary>
        /// Find best class for the blob (i. e. class with maximal probability)
        /// </summary>
        /// <param name="probBlob"></param>
        /// <param name="classId"></param>
        /// <param name="classProb"></param>
        private static void GetMaxClass(Mat probBlob, out int classId, out double classProb)
        {
            // reshape the blob to 1x1000 matrix
            using (var probMat = probBlob.Reshape(1, 1))
            {
                Cv2.MinMaxLoc(probMat, out _, out classProb, out _, out var classNumber);
                classId = classNumber.X;
            }
        }

        public Mat ImageProcess(Mat image, bool display = true)
        {
            int frameHeight = image.Rows;
            int frameWidth = image.Cols;
            // Mat _image_copy = image;
            Mat _face_frame = null;
            Mat _face_frame_copy = null;
            using (var inputBlob = CvDnn.BlobFromImage(image, 1, new Size(300, 300), new Scalar(104.0, 177.0, 123.0), false, false))
            {
                _faceNet.SetInput(inputBlob, "data");
                using (var detections = _faceNet.Forward("detection_out"))
                {
                    // Console.WriteLine(detections.Rows);
                    var detectionMat = new Mat(detections.Size(2), detections.Size(3), MatType.CV_32F, detections.Ptr(0));
                    // Console.WriteLine(detectionMat.Rows);
                    Scalar color = new Scalar(0, 0, 0);
                    string result = string.Empty;
                    for (int i = 0; i < detectionMat.Rows; i++)
                    {
                        float confidence = detectionMat.At<float>(i, 2);
                        int face_box_x1 = 0;
                        int face_box_y1 = 0;
                        int face_box_x2 = 0;
                        int face_box_y2 = 0;

                        /// human face detetction
                        if (confidence > face_confidence)
                        {
                            face_box_x1 = (int)(detectionMat.At<float>(i, 3) * frameWidth);
                            face_box_y1 = (int)(detectionMat.At<float>(i, 4) * frameHeight);
                            face_box_x2 = (int)(detectionMat.At<float>(i, 5) * frameWidth);
                            face_box_y2 = (int)(detectionMat.At<float>(i, 6) * frameHeight);

                            /// mask detection
                            Rect box = new Rect(face_box_x1, face_box_y1, face_box_x2 - face_box_x1, face_box_y2 - face_box_y1);
                            _face_frame = new Mat(image, box);

                            /// Pre-processing
                            // Cv2.CvtColor(_face_frame,_face_frame, ColorConversionCodes.BayerBG2RGB);
                            _face_frame_copy = _face_frame;
                            Cv2.Resize(_face_frame, _face_frame, new Size(224, 224));

                            using (var blob = CvDnn.BlobFromImage(_face_frame, 1, new Size(224, 224), new Scalar(0), true, false))
                            {
                                // -1
                                Mat temp = blob;
                                temp /= 127.5;
                                temp -= 1;
                                _maskNet.SetInput(temp);
                                using (var mask_detections = _maskNet.Forward())
                                {
                                    // Console.WriteLine(maskMat.At<float>(0,0));
                                    // Console.WriteLine(maskMat.At<float>(0,1));
                                    if (mask_detections.At<float>(0, 0) > mask_detections.At<float>(0, 1))
                                    {
                                        result = "with Mask";
                                        color = new Scalar(0, 255, 0);
                                    }
                                    else
                                    {
                                        result = "without Mask";
                                        color = new Scalar(0, 0, 255);
                                    }
                                }
                            }
                            _faceCount++;
                            if (display)
                            {
                                Cv2.Rectangle(image, new Point(face_box_x1, face_box_y1), new Point(face_box_x2, face_box_y2), color);
                                Cv2.Rectangle(image, new Point(face_box_x1, face_box_y1 - 40), new Point(face_box_x2, face_box_y1), color, -1);
                                Cv2.PutText(image, result, new Point(face_box_x1 + 2, face_box_y1 - 10),
                                HersheyFonts.HersheyComplexSmall, 1, Scalar.White, 1);
                            }
                        }
                    }
                    if (_last_faceCount != _faceCount) // judge if changing
                        _withoutMaskSignal = true;

                    if (result == "without Mask")
                    {
                        _withoutMaskCount += 1;
                        if (_withoutMaskSignal)
                            Snapshot(_face_frame_copy); // snapshot
                    }
                    /// Calculate fps

                    if (display)
                    {
                        // Cv2.PutText(image, "fps: " + fps, new Point(w-80, 10),
                        //       HersheyFonts.HersheyComplexSmall, 0.8, Scalar.DarkRed, 1);
                        Cv2.PutText(image, "Face Count: " + GetFaceCount(), new Point(10, 10),
                            HersheyFonts.HersheyComplexSmall, 0.8, Scalar.DarkRed, 1);
                        Cv2.PutText(image, "Without Mask: Count: " + GetWithoutMaskCount(), new Point(10, 30),
                            HersheyFonts.HersheyComplexSmall, 0.8, Scalar.DarkRed, 1);
                    }

                    if (_withoutMaskSignal)
                        _withoutMaskSignal = false;
                    _last_faceCount = _faceCount;
                    _last_withoutMaskCount = _withoutMaskCount;
                    _withoutMaskCount = 0;
                    _faceCount = 0;
                }
            }
            return image;
        }
        private void Snapshot(Mat image, string folder = "null")
        {
            string _daytime = string.Format("{0:yyyyMMddHHmmssff}", DateTime.Now);
            try
            {
                if (folder == "null")
                    folder = Path.Combine(Directory.GetCurrentDirectory(), "snapshot");

                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                string path = Path.Combine(folder, _daytime + ".jpg");
                Cv2.ImWrite(path, image); //for debug
            }
            catch
            {
                Console.WriteLine("check your snapshot's path");
            }
        }
        public int GetFaceCount()
        {
            return _faceCount;
        }
        public int GetWithoutMaskCount()
        {
            return _last_withoutMaskCount;
        }
    }
}